﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuggyProgram
{
    class DataModifier
    {
        int InputValue;
        Random rand = new Random();

        public DataModifier(int Value)
        {
            InputValue = Value;
        }

        public void Modify(int Min, int Max)
        {
            InputValue *= InputValue + rand.Next(Min, Max);
        }

        public int ReturnValue()
        {
            if (InputValue >= 50000)
            {
                return InputValue;
            }

            if (InputValue == 100)
            {
                return 1;
            }

            if (InputValue < 50000)
            {
                return 0;
            }

            return 2;
            
        }

    }
}
